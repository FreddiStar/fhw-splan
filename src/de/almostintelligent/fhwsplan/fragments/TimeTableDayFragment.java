package de.almostintelligent.fhwsplan.fragments;


import de.almostintelligent.fhwsplan.R;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TimeTableDayFragment extends android.support.v4.app.Fragment
{
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstaceState)
	{
		return inflater.inflate(R.layout.placeholder, container, false);
	}
}
